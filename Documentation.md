# Mediation

## Introduction

Mediation is the module that will enable you to display Banners, Interstitals and Rewarded Ads into your game.

## Integration Steps

1) **"Install"** or **"Upload"** FG Mediation plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Scripting API

> Interstitial ads take the whole screen they are often put at the end of the level. Here is an example on how to use it :

```csharp
FGMediation.ShowInterstitial("MyInterstitialAd");
```

> Banner ads appear at the bottom of the screen, it is often displayed in the menu. In order to integrate it into your game you will have to use the following function :

```csharp
FGMediation.ShowBanner();
FGMediation.HideBanner();
```

> Rewarded ads are used when you want to reward the user for watching ads. This advertising format allows to retrieve the state of the completion of the advertisement via callback. Here is an example of implementation :

```csharp
FGMediation.ShowRewarded(EarnExtraCoins,"ExtraCoinRewardedAd");
private void EarnExtraCoins(bool success)
{
  if (success)
  {
    // Give reward !
  }
}
```

> You can also subscribe to these callbacks to attach custom actions to Ad events and Impressions:

```csharp
FGMediation.Callbacks.Initialization;
FGMediation.Callbacks.OnInitialized;

FGMediation.Callbacks.ShowBanner;
FGMediation.Callbacks.HideBanner;
FGMediation.Callbacks.OnBannerAdDisplayed;
FGMediation.Callbacks.OnBannerAdClicked;
FGMediation.Callbacks.OnBannerAdFailedToLoad;
FGMediation.Callbacks.OnBannerAdImpression;

FGMediation.Callbacks.ShowInterstitial;
FGMediation.Callbacks.OnInterstitialAdLoaded; 
FGMediation.Callbacks.OnInterstitialAdDisplayed; 
FGMediation.Callbacks.OnInterstitialClosed; 
FGMediation.Callbacks.OnInterstitialAdClicked; 
FGMediation.Callbacks.OnInterstitialAdImpression; 
FGMediation.Callbacks.OnInterstitialAdFailedToLoad; 
FGMediation.Callbacks.OnInterstitialAdFailedToDisplay; 

FGMediation.Callbacks.ShowRewarded;
FGMediation.Callbacks.OnRewardedAdLoaded; 
FGMediation.Callbacks.OnRewardedAdDisplayed; 
FGMediation.Callbacks.OnRewardedAdClosed;
FGMediation.Callbacks.OnRewardedAdClicked; 
FGMediation.Callbacks.OnRewardedAdImpression; 
FGMediation.Callbacks.OnRewardedAdRewardReceived; 
FGMediation.Callbacks.OnRewardedAdFailedToLoad;
FGMediation.Callbacks.OnRewardedAdFailedToDisplay;
```